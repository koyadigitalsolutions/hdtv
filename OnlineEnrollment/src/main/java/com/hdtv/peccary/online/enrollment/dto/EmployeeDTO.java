package com.hdtv.peccary.online.enrollment.dto;

import java.time.LocalDate;
import java.util.List;

public class EmployeeDTO extends PersonDTO {

	private double salary;
	private String status;
	private LocalDate joinDate;
	private String employement;
	
	private List<AddressDTO> address;
	
	private List<DependentDTO> dependents;
	
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public LocalDate getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(LocalDate joinDate) {
		this.joinDate = joinDate;
	}
	public String getEmployement() {
		return employement;
	}
	public void setEmployement(String employement) {
		this.employement = employement;
	}
	public List<AddressDTO> getAddress() {
		return address;
	}
	public void setAddress(List<AddressDTO> address) {
		this.address = address;
	}
	public List<DependentDTO> getDependents() {
		return dependents;
	}
	public void setDependents(List<DependentDTO> dependents) {
		this.dependents = dependents;
	}
}
