package com.hdtv.peccary.online.enrollment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineEnrollmentApplication {

	public static void main(String[] args) {
        SpringApplication.run(OnlineEnrollmentApplication.class, args);
    }
}
