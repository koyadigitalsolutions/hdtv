package com.hdtv.peccary.online.enrollment.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hdtv.peccary.online.enrollment.exception.RecordNotFoundException;
import com.hdtv.peccary.online.enrollment.repository.Employee;
import com.hdtv.peccary.online.enrollment.repository.EmployeeRepository;

@Service
public class EmployeeService implements IEmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;

	@Override
	public Employee getEmployeeById(Long employeeID) throws RecordNotFoundException {
		Optional<Employee> employee = employeeRepository.findById(employeeID);
		if(employee.isPresent())
			return employee.get();
		else {
			throw new RecordNotFoundException("No employee record exist for given id");
		}
	}

	@Override
	public Employee createOrUpdateEmployee(Employee employee) throws RecordNotFoundException {
		Optional<Employee> employeeEntity = employeeRepository.findById(employee.getEmployeeID());
		if(employeeEntity.isPresent()) {
			Employee newEntity = employeeEntity.get();
			newEntity.setFirstName(employee.getFirstName());
			newEntity.setLastName(employee.getLastName());
			newEntity = employeeRepository.save(newEntity);
			return newEntity;
		}else {
			return employeeRepository.save(employee);
		}
	}

}
