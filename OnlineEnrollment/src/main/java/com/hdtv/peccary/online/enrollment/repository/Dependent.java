package com.hdtv.peccary.online.enrollment.repository;

import java.sql.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DEPENDENT")
public class Dependent {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="DEPENDENT_ID")
	private Long dependentID;
	
	@Column(name="FIRST_NAME")
	private String firstName;
	
	@Column(name="LAST_NAME")
	private String lastName;
	
	@Column(name="SUFFIX")
	private String suffix;
	
	@Column(name="MIDDLE_NAME")
	private String middleName;
	
	@Column(name="GENDER")
	private String gender;
	
	@Column(name="MARITAL_STATUS")
	private String maritalStatus;
	
	@Column(name="DATE_OF_BIRTH")
	private Date dateOfBirth;
	
	@Column(name="EMPLOYEE_STATUS")
	private String status;
	
	@Column(name="DATE_OF_JOIN")
	private Date dateOfJoin;
	
	@Column(name="SSN")
	private String ssn;

	@Column(name="ALTERNATE_ID")
	private String alternateID;

	public Long getDependentID() {
		return dependentID;
	}

	public void setDependentID(Long dependentID) {
		this.dependentID = dependentID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateOfJoin() {
		return dateOfJoin;
	}

	public void setDateOfJoin(Date dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getAlternateID() {
		return alternateID;
	}

	public void setAlternateID(String alternateID) {
		this.alternateID = alternateID;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dependent dependent = (Dependent) o;
        return Objects.equals(dependentID, dependent.dependentID) &&
                Objects.equals(firstName, dependent.firstName) &&
                Objects.equals(lastName, dependent.lastName);
                
    }

    @Override
    public int hashCode() {
        return Objects.hash(dependentID, firstName, lastName);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id=").append(dependentID);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
