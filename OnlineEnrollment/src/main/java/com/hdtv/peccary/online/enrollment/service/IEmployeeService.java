package com.hdtv.peccary.online.enrollment.service;

import com.hdtv.peccary.online.enrollment.exception.RecordNotFoundException;
import com.hdtv.peccary.online.enrollment.repository.Employee;

public interface IEmployeeService {

	public Employee getEmployeeById(Long employeeID) throws RecordNotFoundException;
	
	public Employee createOrUpdateEmployee(Employee employee) throws RecordNotFoundException;
}
