DROP TABLE IF EXISTS EMPLOYEES;
 
CREATE TABLE EMPLOYEES (
  EMPLOYEE_ID INT AUTO_INCREMENT  PRIMARY KEY,
  FIRST_NAME VARCHAR(250) NOT NULL,
  LAST_NAME VARCHAR(250) NOT NULL,
  SUFFIX VARCHAR(10) DEFAULT NULL,
  MIDDLE_NAME VARCHAR(250) DEFAULT NULL,
  GENDER VARCHAR(1) DEFAULT NULL,
  MARITAL_STATUS VARCHAR(10) DEFAULT NULL,
  DATE_OF_BIRTH DATE,
  EMPLOYEE_STATUS VARCHAR(32) DEFAULT NULL,
  DATE_OF_JOIN DATE,
  SSN VARCHAR(12) DEFAULT NULL,
  ALTERNATE_ID VARCHAR(32) DEFAULT NULL
  
);